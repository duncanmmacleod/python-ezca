from setuptools import setup

version = {}
with open("ezca/_version.py") as f:
    exec(f.read(), version)

setup(
    name = 'ezca',
    version = version['__version__'],
    description = 'aLIGO CDS Python EPICS interface',
    author = 'Jameson Graef Rollins',
    author_email = 'jameson.rollins@ligo.org',
    url = 'https://git.ligo.org/cds/python-ezca',
    license = 'GPL v3+',
    keywords = [],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
    ],

    packages = [
        'ezca',
        'ezca.emulators',
    ],

    package_data = {'ezca.emulators': ['TEST_FILTER.adl']}

)
